

#include <err.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <xenctrl.h>
#include <xenguest.h>
#include <xc_private.h>
#include <inttypes.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <string.h>


#define MSR_RAPL_POWER_UNIT		0x606

/*
 * Platform specific RAPL Domains.
 * Note that PP1 RAPL Domain is supported on 062A only
 * And DRAM RAPL Domain is supported on 062D only
 */
/* Package RAPL Domain */
#define MSR_PKG_RAPL_POWER_LIMIT	0x610
#define MSR_PKG_ENERGY_STATUS		0x611
#define MSR_PKG_PERF_STATUS		0x613
#define MSR_PKG_POWER_INFO		0x614

/* PP0 RAPL Domain */
#define MSR_PP0_POWER_LIMIT		0x638
#define MSR_PP0_ENERGY_STATUS		0x639
#define MSR_PP0_POLICY			0x63A
#define MSR_PP0_PERF_STATUS		0x63B

/* PP1 RAPL Domain, may reflect to uncore devices */
#define MSR_PP1_POWER_LIMIT		0x640
#define MSR_PP1_ENERGY_STATUS		0x641
#define MSR_PP1_POLICY			0x642

/* DRAM RAPL Domain */
#define MSR_DRAM_POWER_LIMIT		0x618
#define MSR_DRAM_ENERGY_STATUS		0x619
#define MSR_DRAM_PERF_STATUS		0x61B
#define MSR_DRAM_POWER_INFO		0x61C

/* RAPL UNIT BITMASK */
#define POWER_UNIT_OFFSET	0
#define POWER_UNIT_MASK		0x0F

#define ENERGY_UNIT_OFFSET	0x08
#define ENERGY_UNIT_MASK	0x1F00

#define TIME_UNIT_OFFSET	0x10
#define TIME_UNIT_MASK		0xF000
#define PKG_POWER_LIMIT_LOCK_OFFSET 0x3F
#define PKG_POWER_LIMIT_LOCK_MASK 0x1
#define ENABLE_LIMIT_2_OFFSET 0x2F
#define ENABLE_LIMIT_2_MASK 0x1
#define PKG_CLAMPING_LIMIT_2_OFFSET 0x30
#define PKG_CLAMPING_LIMIT_2_MASK 0x1
#define PKG_POWER_LIMIT_2_OFFSET 0x20
#define PKG_POWER_LIMIT_2_MASK 0x7FFF
#define ENABLE_LIMIT_1_OFFSET 0xF
#define ENABLE_LIMIT_1_MASK 0x1
#define PKG_CLAMPING_LIMIT_1_OFFSET 0x10
#define PKG_CLAMPING_LIMIT_1_MASK 0x1
#define PKG_POWER_LIMIT_1_OFFSET 0x0
#define PKG_POWER_LIMIT_1_MASK 0x7FFF
#define TIME_WINDOW_POWER_LIMIT_1_OFFSET 0x11
#define TIME_WINDOW_POWER_LIMIT_1_MASK 0x7F
#define TIME_WINDOW_POWER_LIMIT_2_OFFSET 0x31
#define TIME_WINDOW_POWER_LIMIT_2_MASK 0x7F





void set_power_limit(xc_interface *xch, int watts, double pu)
{
/*
	uint32_t setpoint = (uint32_t) ((1 << pu) * watts);
	uint64_t reg = 0;
	rdmsr(fd, MSR_PKG_RAPL_POWER_LIMIT, &reg);
	reg = (reg & 0xFFFFFFFFFFFF0000) | setpoint | 0x8000;
	reg = (reg & 0xFFFFFFFF0000FFFF) | 0xD0000;
	wrmsr(fd, MSR_PKG_RAPL_POWER_LIMIT, reg);
*/

	uint32_t setpoint = (uint32_t) (watts/pu);
	uint64_t reg = 0;
	
	printf("\n\nThe setpoint is %"PRIu32"\n", setpoint);
	do_xempower_hypercall(xch, MSR_PKG_RAPL_POWER_LIMIT, &reg, RDMSR_HYPERCALL);
	reg = (reg & 0xFFFFFFFFFFFF0000) | setpoint | 0x8000;
	reg = (reg & 0xFFFFFFFF0000FFFF) | 0xD0000;
	do_xempower_hypercall(xch, MSR_PKG_RAPL_POWER_LIMIT, &reg, WRMSR_HYPERCALL);
            


}






int main(int argc, char **argv)
{
	xc_interface *xch;
	long ret;
	uint64_t result = 123;
	double power_units,energy_units,time_units;
	uint64_t power_target;

	if ( (argc != 2) )
		errx(1, "usage: %s power_limit[Watt] ", argv[0]);

	xch = xc_interface_open(0,0,0);
	if ( !xch ){
		errx(1, "xcutils: xc_xempower_wrmsr.c: failed to open control interface");
	}



	
	power_target= atoi(argv[1]);
  	printf("\n\nStarting to set package power to %"PRIu64"\n",power_target);

 	
  	ret = do_xempower_hypercall(xch, MSR_RAPL_POWER_UNIT, &result, RDMSR_HYPERCALL);
  
  	power_units=pow(0.5,(double)(result&0xf));
  	energy_units=pow(0.5,(double)((result>>8)&0x1f));
  	time_units=pow(0.5,(double)((result>>16)&0xf));

  	set_power_limit(xch, power_target, power_units);

	if ( ret == 0 )
	{
		errx(1, "ret == 0\n");
		fflush(stdout);
	}

	xc_interface_close(xch);

	return ret;
}