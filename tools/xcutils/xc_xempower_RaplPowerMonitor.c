/*#ifdef _POSIX_C_SOURCE
	#undef _POSIX_C_SOURCE
#endif

#define _POSIX_C_SOURCE	199309L
*/

#include <err.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <xenctrl.h>
#include <xenguest.h>
#include <xc_private.h>
#include <inttypes.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>


#define MSR_RAPL_POWER_UNIT		0x606

/*
 * Platform specific RAPL Domains.
 * Note that PP1 RAPL Domain is supported on 062A only
 * And DRAM RAPL Domain is supported on 062D only
 */
/* Package RAPL Domain */
#define MSR_PKG_RAPL_POWER_LIMIT	0x610
#define MSR_PKG_ENERGY_STATUS		0x611
#define MSR_PKG_PERF_STATUS		0x613
#define MSR_PKG_POWER_INFO		0x614

/* PP0 RAPL Domain */
#define MSR_PP0_POWER_LIMIT		0x638
#define MSR_PP0_ENERGY_STATUS		0x639
#define MSR_PP0_POLICY			0x63A
#define MSR_PP0_PERF_STATUS		0x63B

/* PP1 RAPL Domain, may reflect to uncore devices */
#define MSR_PP1_POWER_LIMIT		0x640
#define MSR_PP1_ENERGY_STATUS		0x641
#define MSR_PP1_POLICY			0x642

/* DRAM RAPL Domain */
#define MSR_DRAM_POWER_LIMIT		0x618
#define MSR_DRAM_ENERGY_STATUS		0x619
#define MSR_DRAM_PERF_STATUS		0x61B
#define MSR_DRAM_POWER_INFO		0x61C

/* RAPL UNIT BITMASK */
#define POWER_UNIT_OFFSET	0
#define POWER_UNIT_MASK		0x0F

#define ENERGY_UNIT_OFFSET	0x08
#define ENERGY_UNIT_MASK	0x1F00

#define TIME_UNIT_OFFSET	0x10
#define TIME_UNIT_MASK		0xF000
#define PKG_POWER_LIMIT_LOCK_OFFSET 0x3F
#define PKG_POWER_LIMIT_LOCK_MASK 0x1
#define ENABLE_LIMIT_2_OFFSET 0x2F
#define ENABLE_LIMIT_2_MASK 0x1
#define PKG_CLAMPING_LIMIT_2_OFFSET 0x30
#define PKG_CLAMPING_LIMIT_2_MASK 0x1
#define PKG_POWER_LIMIT_2_OFFSET 0x20
#define PKG_POWER_LIMIT_2_MASK 0x7FFF
#define ENABLE_LIMIT_1_OFFSET 0xF
#define ENABLE_LIMIT_1_MASK 0x1
#define PKG_CLAMPING_LIMIT_1_OFFSET 0x10
#define PKG_CLAMPING_LIMIT_1_MASK 0x1
#define PKG_POWER_LIMIT_1_OFFSET 0x0
#define PKG_POWER_LIMIT_1_MASK 0x7FFF
#define TIME_WINDOW_POWER_LIMIT_1_OFFSET 0x11
#define TIME_WINDOW_POWER_LIMIT_1_MASK 0x7F
#define TIME_WINDOW_POWER_LIMIT_2_OFFSET 0x31
#define TIME_WINDOW_POWER_LIMIT_2_MASK 0x7F



int main(int argc, char **argv)
{
	xc_interface *xch;
	long ret;
	uint64_t result = 123;
	double power_units,energy_units,time_units;
	double package_before,package_after;
	struct  timeval currentime1,currentime2,beginningtime,tmptime;
	struct timespec interval_1s,interval_1ms,interval_10ms, interval_100ms, interval_500ms;
	long  double nowtime, power;
	uint64_t currentval;
	long double SocketPower;
	int i; 
	//FILE *PowerFilePointer;




	if ( (argc != 1) )
		errx(1, "usage: %s ", argv[0]);

	xch = xc_interface_open(NULL,NULL,1);
	if ( !xch ){
		errx(1, "xcutils: xc_xempower_rdmsr.c: failed to open control interface");
	}


	printf("Starting\n");
	
	// char *filename;
	gettimeofday(&beginningtime,NULL);
	printf("beginningtime.tv_sec= %ld \n",beginningtime.tv_sec);
	printf("beginningtime.tv_usec= %ld \n",beginningtime.tv_usec);
	interval_500ms.tv_sec = 0;
	interval_500ms.tv_nsec = 500000000;
	interval_100ms.tv_sec = 0;
	interval_100ms.tv_nsec = 100000000;
	interval_1s.tv_sec = 1;
	interval_1s.tv_nsec = 0;
	interval_1ms.tv_sec = 0;
	interval_1ms.tv_nsec = 1000000;
	interval_10ms.tv_sec = 0;
	interval_10ms.tv_nsec = 10000000;

	/* Calculate the units used */
	ret=do_xempower_hypercall(xch, MSR_RAPL_POWER_UNIT, &result, RDMSR_HYPERCALL);

	printf("result %"PRIu64" \n", result);

	power_units = 3;
	energy_units = 3;
	time_units = 3;
	  
	power_units=pow(0.5,(double)(result&0xf));
	energy_units=pow(0.5,(double)((result>>8)&0x1f));
	time_units=pow(0.5,(double)((result>>16)&0xf));

	printf("The system units are: 1 power unit = %lF W  || 1 energy unit = %lF J || 1 time unit = %lF seconds \n", power_units, energy_units, time_units);


	ret=do_xempower_hypercall(xch, MSR_PKG_RAPL_POWER_LIMIT, &currentval, RDMSR_HYPERCALL);
 
	printf("RAPL power limit1 = %.6fW\n", power_units*(double)(currentval&0x7fff));
	
	//PowerFilePointer = fopen("/home/arnaboldi/log/socket_power.txt","w");
	//fclose(PowerFilePointer);

	i = 0;
	while (i<20)
	{
		i++;
	 	//PowerFilePointer = fopen("/home/arnaboldi/log/socket_power.txt","a");

	  	ret=do_xempower_hypercall(xch, MSR_PKG_ENERGY_STATUS, &result, RDMSR_HYPERCALL);
		package_before=(double)result*energy_units;
	    gettimeofday(&currentime1, NULL);
	  	
	  	tmptime = currentime1;
	  	while(abs(tmptime.tv_usec - currentime1.tv_usec) < 100000)
	  	{
	  		gettimeofday(&tmptime, NULL);
	  	}
		//nanosleep(&interval_100ms, NULL);

	  	ret=do_xempower_hypercall(xch, MSR_PKG_ENERGY_STATUS, &result, RDMSR_HYPERCALL);
	  	gettimeofday(&currentime2, NULL);
	  	
		package_after=(double)result*energy_units;
	 	printf("Package1 energy after: %.6f  (%.6fJ consumed)\n", package_after,package_after-package_before);
	        
		nowtime =((long) ((currentime2.tv_usec - beginningtime.tv_usec) + (currentime2.tv_sec - beginningtime.tv_sec)* 1000000))/1000000.000000;
	    printf("nowtime =%.6LF\n", nowtime);
	  	power =((package_after - package_before) /((currentime2.tv_usec - currentime1.tv_usec) + (currentime2.tv_sec - currentime1.tv_sec)*1000000))*1000000;
	    printf("power1 =%LF\n", power);
	    SocketPower = power;
		//fprintf(PowerFilePointer,"%LF\n",SocketPower);
		sleep(1);
		//fclose(PowerFilePointer);
	}

	/*
	ret = do_xempower_hypercall(xch, MSR_RAPL_POWER_UNIT, &val, RDMSR_HYPERCALL);


	printf("\n\n\n\n\n return value %ld \n\n\n\n\n", val);


	power_units=pow(0.5,(double)(val&0xf));
    energy_units=pow(0.5,(double)((val>>8)&0x1f));
    time_units=pow(0.5,(double)((val>>16)&0xf));



	printf("power units   --> %lf \n", power_units);
	printf("energy units  --> %lf \n", energy_units);
	printf("time units    --> %lf \n", time_units);
	*/

	if ( ret == 0 )
	{
		errx(1, "ret == 0\n");
		fflush(stdout);
		return 0;
	}

	xc_interface_close(xch);

	return 1;
}